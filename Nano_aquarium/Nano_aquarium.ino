#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <DHT.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <brzo_i2c.h>
#include "SSD1306Brzo.h"
#include <ArduinoOTA.h>

#define mqtt_server "192.168.0.2"
#define mqtt_user "homeassistant"
#define mqtt_password "2032"
#define mqtt_client "nano_aquarium_moisture"

#define moisture_topic "sensor/nano_aquarium/moisture"
#define aquarium_temperature_topic "sensor/nano_aquarium/temperature"
#define humidity_topic "sensor/kitchen/humidity"
#define temperature_topic "sensor/kitchen/temperature"

#define DISPLAY_WIDTH 128
#define DISPLAY_HEIGHT 64

#define DHTTYPE DHT11
#define DHTPIN 5
#define INPUT_PIN D4

WiFiClient espClient;
PubSubClient client(espClient);
DHT dht(DHTPIN, DHTTYPE, 11);
OneWire oneWire(INPUT_PIN);
DallasTemperature DS18B20(&oneWire);
SSD1306Brzo display(0x3c, D6, D5);

void setup() {
  Serial.begin(115200);
  dht.begin();
  DS18B20.begin();
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  setup_screen();
  setup_OTA();
}

void setup_OTA() {
  ArduinoOTA.setHostname(mqtt_client);
  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });

  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });

  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });

  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void setup_wifi() {
  WiFiManager wifiManager;
  wifiManager.autoConnect(mqtt_client);
}

void setup_screen() {
  display.init();
  display.flipScreenVertically();
}

void publish_message(String topic, String value) {
  Serial.print("Publishing message with value: ");
  Serial.print(value.c_str());
  Serial.print(" to topic: ");
  Serial.println(topic);
  client.publish(topic.c_str(), value.c_str(), true);
}

bool connected = false;
void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection... ");
    if (client.connect(mqtt_client, mqtt_user, mqtt_password)) {
      Serial.println("connected");
      connected = true;
    } else {
      Serial.print("Failed to connect, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

bool checkBound(int newValue, int prevValue, int maxDiff) {
  return !isnan(newValue) &&
         (newValue < prevValue - maxDiff || newValue > prevValue + maxDiff);
}

bool checkBoundFl(float newValue, float prevValue, float maxDiff) {
  return !isnan(newValue) &&
         (newValue < prevValue - maxDiff || newValue > prevValue + maxDiff);
}

int relativeMoisture = 0;
void readAquariumMoisture() {
  long lastMsg = 0;
  double analogValue = 0.0;
  double analogVolts = 0.0;
  int diff = 2;
  long now = millis();
  if (now - lastMsg > 2000) {
    lastMsg = now;
    Serial.println("Taking Aquarium Mosisture");

    analogValue = analogRead(A0); // read the analog signal

    // convert the analog signal to voltage
    // the ESP2866 A0 reads between 0 and ~3 volts, producing a corresponding value
    // between 0 and 1024. The equation below will convert the value to a voltage value.
    analogVolts = (analogValue * 3.08) / 1024;

    // now get our chart value by converting the analog (0-1024) value to a value between 0 and 100.
    // the value of 400 was determined by using a dry moisture sensor (not in soil, just in air).
    // When dry, the moisture sensor value was approximately 400. This value might need adjustment
    // for fine tuning of the chartValue.
    int newRelativeMoisture = (analogValue * 100) / 965;

    // now reverse the value so that the value goes up as moisture increases
    // the raw value goes down with wetness, we want our chart to go up with wetness
    newRelativeMoisture = 100 - newRelativeMoisture;
    
    if (newRelativeMoisture < 0) {
      newRelativeMoisture = 0;
    }
      
    if (checkBound(newRelativeMoisture, relativeMoisture, diff)) {
      relativeMoisture = newRelativeMoisture;

      // Serial data
      Serial.print("Analog Raw Value: ");
      Serial.println(analogValue);
      Serial.print("Analog Volts: ");
      Serial.println(analogVolts);
      Serial.print("Relative Moisture: ");
      Serial.println(relativeMoisture);
      Serial.println(" ");

      Serial.print("New tank moisture: ");
      Serial.println(String(relativeMoisture).c_str());

      publish_message(moisture_topic, String(relativeMoisture));
    } else {
      Serial.println("Aquarium Moisture unchanged");
    }

    delay(5000);
  }
}

long roomLastMsg = 0;
float roomTemp = 0.0;
float roomHum = 0.0;
float roomDiff = 1.0;
void readTempHumidity() {
  
  long now = millis();
  if (now - roomLastMsg > 2000) {
    roomLastMsg = now;
    Serial.println("Taking Room Humidity and Temperature");

    float newTemp = dht.readTemperature();
    float newHum = dht.readHumidity();

    if (checkBoundFl(newTemp, roomTemp, roomDiff)) {
      roomTemp = newTemp;
      Serial.print("New room temperature: ");
      Serial.println(String(roomTemp).c_str());
      int publishTemp = roomTemp;
      publish_message(temperature_topic, String(publishTemp));
    } else {
      Serial.println("Room Temperature unchanged");
    }

    if (checkBoundFl(newHum, roomHum, roomDiff)) {
      roomHum = newHum;
      Serial.print("New room humidity: ");
      Serial.println(String(roomHum).c_str());
      int publishHum = roomHum;
      publish_message(humidity_topic, String(publishHum));
    } else {
      Serial.println("Room Humidity unchanged");      
    }
  }
}

float aqTemp = 0.0;
long aqLastMsg = 0;
float aqDiff = 0.25;
void readAquariumTemperature() {
  long now = millis();
  if (now - aqLastMsg > 2000) {
    aqLastMsg = now;
    Serial.println("Taking Aquarium Temperature");

    DS18B20.requestTemperatures();
    float newTemp = DS18B20.getTempCByIndex(0);

    if (checkBoundFl(newTemp, aqTemp, aqDiff)) {
      aqTemp = newTemp;
      Serial.print("New aquarium temperature: ");
      Serial.println(String(aqTemp).c_str());
      publish_message(aquarium_temperature_topic, String(aqTemp));
    } else {
      Serial.println("Aquarium Temperature unchanged");       
    }
    delay(5000);
  }
}


String getConnectStatus() {
  if (connected) {
    return "Connected";
  } else {
    return "Disconnected";
  }
}

void drawScreen() {
  Serial.println("Redrawing screen");
  display.clear();
  display.drawRect(0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT);

  display.setFont(ArialMT_Plain_10);
  display.drawString(4, 0, "IP: " + String(WiFi.localIP().toString()));
  display.drawString(4, 15, "MQTT: " + getConnectStatus());
  display.drawString(4, 30, "Aquarium Temp: " + String(aqTemp) + "C");
  display.drawString(4, 45, "Room: " + String((int) roomTemp) + "C   " + String((int) roomHum) + "%");

  display.display();
  yield();
}


void loop() {
  ArduinoOTA.handle();
  if (!client.connected()) {
    connected = false;
    Serial.print("Not connected to MQTT. State: ");
    Serial.println(client.state());
    reconnect();
  }
  client.loop();

  drawScreen();

  readTempHumidity();
  readAquariumMoisture();
  readAquariumTemperature();

  delay(2000);
}
